# <center><a rel="noopener noreferrer" target="_blank" href="https://www.quentingenet.fr">Quentin Genet</a></center>

## About me
Hello, I'm Quentin, a french developer specialised in Java back-end developpement.<br>
I'm interested in GNU/Linux, E-Health, and opensource.<br>
Currently, i'm working for a french company which develop softwares for french public hospitals.

## Technicals skills  
* <strong>BACK-END DEVELOPMENT</strong><br>
 `Java native`<br>
`Spring framework : Spring Boot, Spring Security, Spring Data/JPA`<br>
`Docker and Docker Compose`<br>
`Maven`

* <strong>FRONT-END DEVELOPMENT</strong><br>
`React.JS, React native`<br>
`Html, CSS/SCSS`<br>
`Bootstrap`

* <strong>TESTING</strong><br>
`Junit 5`<br>
`Jacoco`

* <strong>DATABASE MANAGEMENT</strong><br>
`MariaDB/MySQL`<br>
`PostgreSQL`<br>
 `Hibernate` <br>

---------------------------------------------------------------------------------------

#### Please have a look on my last personal web development project : 
# <center><a rel="noopener noreferrer" target="_blank" href="https://gitlab.com/genetquentin/openweighttracker_backend"><strong>OpenWeightTracker</strong></a></center>

<center>OpenWeightTracker (OWT)<br>is a free and opensource application under Copyleft.<br>
This is a simple and easy way to have a look on your  weight !<br>
It respect your data, and respect your privacy.</center>
